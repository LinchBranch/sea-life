//
//  MapCollectionViewCell.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class MapCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ImageView: UIImageView!
    
    func setImage(image: UIImage) {
        ImageView.image = image
    }
}
