//
//  GameManager.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class GameManager: NSObject {
    
    let mapWidth: Int = 10
    let mapHeight: Int = 15
    
    let penguinProbability = 0.5
    let orcaProbability = 0.05
    
    var map: MapProtocol!
    var gameMap = [MapCell]()
    
    func startGame() -> [MapCell] {
        
        map = MapFactory.create(width: mapWidth, height: mapHeight)
        gameMap = map.getMap()
        
        var penguinCount = Int(Double(mapWidth * mapHeight) * penguinProbability)
        var orcaCount = Int(Double(mapWidth * mapHeight) * orcaProbability)
        var nullCount = (mapWidth * mapHeight) - (penguinCount + orcaCount)
        
        for i in 0 ..< gameMap.count {
            let random = Int.random(in: 0 ..< (penguinCount + orcaCount + nullCount))
            if random < orcaCount {
                gameMap[i].animal = Orca()
                gameMap[i].canMove = true
                orcaCount -= 1
            } else if random < penguinCount + orcaCount && random >= orcaCount {
                penguinCount -= 1
                gameMap[i].animal = Penguin()
                gameMap[i].canMove = true
            } else {
                nullCount -= 1
            }
        }
        return gameMap
        
    }
    
    func step() {
        
        let animals = gameMap.filter { (cell) -> Bool in
            if cell.animal != nil {
                cell.canMove = true
                return true
            }
            return false
        }
        
        if !animals.isEmpty {
            for i in 0 ..< gameMap.count where gameMap[i].canMove {
                
                var surroundings = map.getSurroundingsCells(index: i)
                var newPosition = gameMap[i].animal?.move(surroundings: surroundings)
                
                if let cell = newPosition {
                    cell.animal = gameMap[i].animal
                    gameMap[i].animal = nil
                    cell.canMove = false
                    gameMap[i].canMove = false
                } else {
                    newPosition = gameMap[i]
                    newPosition?.canMove = false
                }
                
                guard let index = map.getIndex(cell: newPosition!) else {
                    print("cell not found in map")
                    return
                }
                
                surroundings = map.getSurroundingsCells(index: index)
                let childPosition = newPosition?.animal?.reproduction(surroundings: surroundings)
                if let cell = childPosition {
                    cell.animal = newPosition!.animal?.createChild()
                }
                
                if newPosition!.animal!.death() {
                    newPosition?.animal = nil
                }
            }
        }
        
    }
    
}
