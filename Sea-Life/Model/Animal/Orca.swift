//
//  Orca.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class Orca: Animal {
    
    var hunger = Int()
    
    override init() {
        super.init()
        self.reproductionPeriod = 8
        hunger = 0
    }
    
    override func move(surroundings: [MapCell?]) -> MapCell? {
        hunger += 1
        let newCell = super.move(surroundings: surroundings)
        
        if let cell = newCell {
            if cell.animal != nil {
                hunger = 0
            }
        }
        return newCell
    }
    
    override func canEat(animal: Animal) -> Bool {
        return animal is Penguin
    }
    
    override func createChild() -> Animal {
        return Orca()
    }
    
    override func death() -> Bool {
        return hunger >= 3
    }
}
