//
//  Animal.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class Animal: NSObject {
    
    var age = Int()
    var reproductionPeriod = Int()
 
    override init() {
        age = 0
    }
    
    func move(surroundings: [MapCell?]) -> MapCell? {
        
        age += 1
        
        let allFoodCells = surroundings.filter { (surroundingsCell) -> Bool in
            if let cell = surroundingsCell {
                return cell.animal != nil && canEat(animal: cell.animal!)
            }
            return false
        }
        
        if !allFoodCells.isEmpty {
            return allFoodCells.randomElement()!
        }
        
        let randomDirection = surroundings.randomElement()!
        if let direction = randomDirection {
            if direction.animal == nil {
                return direction
            }
        }
        
        return nil
        
    }
    
    func reproduction(surroundings: [MapCell?]) -> MapCell? {
        if self.age % reproductionPeriod == 0 {
            let freeSurroundings = surroundings.filter { (surroundingsCell) -> Bool in
                if let cell = surroundingsCell {
                    if cell.animal == nil {
                        return true
                    }
                }
                return false
            }
            if !freeSurroundings.isEmpty {
                let randomCell = freeSurroundings[Int.random(in: 0 ..< freeSurroundings.count)]
                return randomCell
            }
        }
        return nil
    }
    
    func createChild() -> Animal {
        return Animal()
    }
    
    func canEat(animal: Animal) -> Bool {
        return false
    }
    
    func death() -> Bool{
        return false
    }
}
