//
//  Penguin.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class Penguin: Animal {
    
    override init() {
        super.init()
        self.reproductionPeriod = 3
    }
    
    override func createChild() -> Animal {
        return Penguin()
    }
    
}
