//
//  MapFabric.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

public class MapFactory: NSObject {
    
    static func create(width: Int, height: Int) -> MapProtocol {
        return Map2D(mapWidth: width, mapHeight: height)
    }
}
