//
//  MapProtocol.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol MapProtocol {
    
    func getMap() -> [MapCell]

    func getSurroundingsCells(index: Int) -> [MapCell?]
    
    func getIndex(cell: MapCell) -> Int?
    
}
