//
//  Map2D.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class Map2D: MapProtocol {
    
    var mapWidth: Int!
    var mapHeight: Int!
    var map = [MapCell]()
    
    init(mapWidth: Int, mapHeight: Int) {
        self.mapWidth = mapWidth
        self.mapHeight = mapHeight
        
        for _ in 0 ..< mapWidth * mapHeight {
            let cell = MapCell()
            map.append(cell)
        }
    }
    
    func getMap() -> [MapCell] {
        return map
    }
    
    func getSurroundingsCells(index: Int) -> [MapCell?] {
        var surroundings = [MapCell?]()
        let positionX = index / mapWidth
        let positionY = index % mapWidth
        for i in positionX - 1 ... positionX + 1 {
            for j in positionY - 1 ... positionY + 1 {
                let currentIndex = i * mapWidth + j
                
                if i >= 0 && j >= 0 && i < mapHeight && j < mapWidth {
                    if currentIndex != index {
                        surroundings.append(map[i * mapWidth + j])
                    }
                } else {
                    surroundings.append(nil)
                }
            }
        }
        return surroundings
    }
    
    func getIndex(cell: MapCell) -> Int? {
        return map.firstIndex(of: cell)
    }
}
