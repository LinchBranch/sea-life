//
//  MapCell.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class MapCell: NSObject {
    
    var canMove: Bool = false
    var animal: Animal?
    
}
