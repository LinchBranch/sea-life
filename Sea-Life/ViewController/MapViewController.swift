//
//  ViewController.swift
//  Sea-Life
//
//  Created by Alex on 23/11/2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    @IBOutlet weak var restartButton: UIButton!
    var game = GameManager()
    var map: [MapCell]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        game = GameManager()
        map = game.startGame()
    }

    @IBAction func restart(_ sender: Any) {
        map = game.startGame()
        collectionView.reloadData()
    }
}

extension MapViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return map.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mapCell",
                                                         for: indexPath) as? MapCollectionViewCell {
            if let _ = map[indexPath.row].animal as? Penguin {
                cell.ImageView.image = UIImage(named: "tux")
            } else if let _ = map[indexPath.row].animal as? Orca {
                cell.ImageView.image = UIImage(named: "orca")
            } else {
                cell.ImageView.image = UIImage()
            }
            cell.ImageView.layer.borderColor = UIColor.white.cgColor
            cell.ImageView.layer.borderWidth = 1.0
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        game.step()
        collectionView.reloadData()
    }
    
}

extension MapViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Int(self.collectionView.bounds.width / CGFloat(game.mapWidth) ),
                      height: Int(self.collectionView.bounds.height / CGFloat(game.mapHeight) ))
    }
    
}

